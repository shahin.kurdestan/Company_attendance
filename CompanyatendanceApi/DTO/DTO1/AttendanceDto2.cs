﻿public class AttendanceDto2
{
    public int EmployeeId { get; set; }
    public DateTime CheckIn { get; set; }
    public DateTime CheckOut { get; set; }
}